# -*- coding: utf-8 -*-

import os
import xml.etree.cElementTree as ET


def main():
	XML_FILE = os.path.join(os.getcwd(), 'sample.xml')

	try:
	    tree = ET.ElementTree(file=XML_FILE)
	    # print(help(tree))
	    print(tree)
	    print(tree.getroot())

	    for child_of_root in tree.getroot():
        	print("\t", child_of_root.tag, child_of_root.attrib)

        	for second_level_child in child_of_root:
        		print("\t\t2 level: {} {}".format(second_level_child.tag, second_level_child.attrib))

	except IOError as e:
	    print('nERROR - cant find file: %sn' % e)


if __name__ == "__main__":
	main()
