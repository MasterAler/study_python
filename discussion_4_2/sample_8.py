# -*- coding: utf-8 -*-

import os
import struct
import pefile #pip install --pre pype32

# from macholib.MachO import MachO
# http://kaitai.io/#quick-start
# https://www.carvesystems.com/news/parsing-binaries-with-kaitai-struct/

def binary_experiments():
	# magic = b'\xcf\xfa\xed\xfe'
	magic = b'\x01\x00\x00\x00'

	decoded = struct.unpack('<I', magic)[0]
	print(struct.calcsize('<I'))
	print(hex(decoded))	
	print(int(decoded))


def parse_calc_exe():
	pe = pefile.PE(os.path.join(os.getcwd(), 'calc.exe'))

	for section in pe.sections:
		print(section.Name, hex(section.VirtualAddress), hex(section.Misc_VirtualSize), section.SizeOfRawData )

	print(pe.sections[0].get_data()[:10])


def main():
	parse_calc_exe()


if __name__ == "__main__": 
	main()
