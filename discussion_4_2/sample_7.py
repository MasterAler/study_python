# -*- coding: utf-8 -*-

import os
import json

FILENAME = 'my_data.json'

def do_some_crazy_logic(flag, lol, smth, numeric, value, nothing):
	if flag:
		print(value * numeric)

	if nothing is None:
		for i in lol:
			print("{}\t{}".format(smth, i))


def write_sample_data():
	sample_data = {
		"lol": [1, 2, 3],
		"value": 23,
		"smth": "BlaBla",
		"flag": True,
		"numeric": 22.3,
		"nothing": None
	}

	with open(os.path.join(os.getcwd(), FILENAME), "w") as f:
		f.write(json.dumps(sample_data, sort_keys=True, indent=4))


def read_sample_data():
	input_data = None
	with open(os.path.join(os.getcwd(), FILENAME), "r") as f:
		input_data = json.loads("\n".join(f.readlines()))

	return input_data

######

def gen_array():
	arr = []
	for i in range(1, 4):
		arr.append({"x": i, "y": i**2})

	with open(os.path.join(os.getcwd(), "other.xml"), "w") as f:
		f.write(json.dumps(arr, indent = 4))


def print_array(arr):
	for i in arr:
		print("x= {}\t y={}".format(i["x"], i["y"]))


def read_array():
	result = None
	with open(os.path.join(os.getcwd(), "other.xml"), "r") as f:
		result = json.load(f)

	return result

def get_some_calc(a, b, c):
	print(a["x"] * b["x"] * c["x"] + a["y"] * b["y"] * c["y"])


def main():
	write_sample_data()
	my_data = read_sample_data()
	do_some_crazy_logic(**my_data)

	gen_array()
	my_array = read_array()
	print_array(my_array)
	get_some_calc(*my_array)

	print("DONE")


if __name__ == "__main__": 
	main()
