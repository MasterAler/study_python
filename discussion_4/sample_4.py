# -*- coding: utf-8 -*-

def launch_with_try(fn):
	def wrapper(*args):
		try:
			fn(*args)
		except Exception as e:
			print(str(e))
	return wrapper


@launch_with_try
def lame_func():
	for i in range(5):
		if i == 3:
			raise Exception("LOL")
		else:
			print(i)


def my_first_decorator(some_func):
	'''This is a decorator'''
	def my_first_wrapper():
		print("Wrapper function started")
		some_func()
		print("Wrapper function ended")

	return my_first_wrapper


@my_first_decorator
def hello_world():
	print("Hello World!")


def do_whatever(some_func):
	print("Now goes some_func")
	some_func()	


def complex_whatever():
	print("==1==")

	def say_hello_inner(inner_var):
		print("==inner==")
		print("Hello World var = {}".format(inner_var))

	print("==2==")
	return say_hello_inner


def main():
	#1
	# azazaz = hello_world
	# azazaz()

	#2
	# do_whatever(hello_world)

	#3
	# var_sayer = complex_whatever()
	# var_sayer(12)

	#4
	hello_world()

	# lame_func()


if __name__ == "__main__":
	main()
