# -*- coding: utf-8 -*-

def all_on_list():
	list_a = ["Hello", "World", "In", "Python"]
	small_list_a = [str.lower() for str in list_a]
	print(small_list_a)


def with_condition():
	list_a = [1, 2, 3, 4]
	list_b = [2, 3, 4, 5]
	common_num = []

	for a in list_a:
		for b in list_b:
			if a == b:
				common_num.append(a)

	# common_num = [a for a in list_a for b in list_b if a == b]
	# different_num = [(a, b) for a in list_a for b in list_b if a != b
	print(common_num) 


def dict_compreh():
	my_dict = {'a': 1, 'b': 2, 'c': 3, 'd': 4, 'e': 5 }

	double_dict = {k:v*2 for (k,v) in my_dict.items()}
	print(double_dict)


def dict_compreh_with_clause():
	numbers = range(10)
	new_dict_comp = {n:n**2 for n in numbers if n%2 == 0}
	print(new_dict_comp)

	dict1 = {'a': 1, 'b': 2, 'c': 3, 'd': 4, 'e': 5}
	dict1_doubleCond = {k:v for (k,v) in dict1.items() if v > 2 if v % 2 == 0}
	print(dict1_doubleCond)

	dict1_tripleCond = {k:('even' if v % 2 == 0 else 'odd') for (k,v) in dict1.items()}
	print(dict1_tripleCond)
	

def main():
	a = [i**2 for i in range(10)]
	print(a)

	# all_on_list()
	# with_condition()
	# dict_compreh()
	# dict_compreh_with_clause()


if __name__ == "__main__":
	main()
