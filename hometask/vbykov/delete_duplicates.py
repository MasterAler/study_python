import random


def gen_random_item():
    for x in range(10):
        return random.randint(1, 30)


def gen_random_list():
    random_list = []
    for item in range(200):
        random_list.append(gen_random_item())
    print(random_list, '\n')
    return random_list


def delete_duplicates(li):
    s = []
    for item in li:
        if item not in s:
            s.append(item)
    print(s, '\n')
    return s


def main():
    delete_duplicates(gen_random_list())


if __name__ == '__main__':
    main()
