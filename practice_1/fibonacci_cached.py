# -*- coding: utf-8 -*-

import time

def timeit(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        if 'log_time' in kw:
            name = kw.get('log_name', method.__name__.upper())
            kw['log_time'][name] = int((te - ts) * 1000)
        else:
            print('%r  %2.2f ms' % \
                  (method.__name__, (te - ts) * 1000))
        return result
    return timed

###########################################################################

def fibonacci_lame(n):
	if n == 0:
		return 0
	if n == 1:
		return 1

	return fibonacci_lame(n - 2) + fibonacci_lame(n - 1)


def fibonacci(n):
	if n == 0:
		return 0
	if n == 1:
		return 1

	if n in fibonacci.cache:
		return fibonacci.cache[n]
	
	if not (n - 1 in fibonacci.cache):
		fibonacci.cache[n - 1] = fibonacci(n - 1) 

	if not (n - 2 in fibonacci.cache):
		fibonacci.cache[n - 2] = fibonacci(n - 2) 

	return fibonacci.cache[n - 1] + fibonacci.cache[n - 2] 


fibonacci.cache = {}

###########################################################################

@timeit
def call_fib_good(n):
	return fibonacci(n)


@timeit
def call_fib_lame(n):
	return fibonacci_lame(n)

def main():
	print(call_fib_lame(30))
	print(call_fib_good(300))


if __name__ == "__main__":
	main()
