# -*- coding: utf-8 -*-

import random


def binary_search(some_data, item):
	start = 0
	end = len(some_data)

	while start < end:
		pos = start + (end - start) // 2
		if some_data[pos] == item:
			return pos
		else:
			if some_data[pos] < item:
				start = pos
			else: 
				end = pos

	return pos


def main():
	data = []
	N = 20
	for x in range(N):
		data.append(random.randint(-200, 200))

	target = -9 
	data.insert(random.randint(0, N), target)

	data = sorted(data)

	for i in data:
		print(i)

	print("_____________________")

	pos = binary_search(data, target)
	print(pos + 1)


if __name__ == "__main__":
	main()
