# -*- coding: utf-8 -*-

import logging

class SomeMagicTestCase:

	__LOG_FILE_NAME = "logs.txt"

	def __init__(self, defaultval = 444):
		self.__a = defaultval

		self.__logger = logging.getLogger('study_logger')
		self.__logger.setLevel(logging.INFO)

		fh = logging.FileHandler(SomeMagicTestCase.__LOG_FILE_NAME)
		fh.setLevel(logging.INFO)

		formatstr = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
		formatter = logging.Formatter(formatstr)
		fh.setFormatter(formatter)

		self.__logger.addHandler(fh)


	def say_hello(fubar):
		print("Hello World {}\t contents: {}".format(fubar.__a, str(dir(fubar))))

	def do_magic(self):
		print("Magic!")

	# def change_log_file_name2(new_log_filename):
	# 	SomeMagicTestCase.__LOG_FILE_NAME = new_log_filename

	@classmethod
	def change_log_file_name(cls, new_log_filename):
		cls.__LOG_FILE_NAME = new_log_filename

	@classmethod
	def get_log_file_name(cls):
		return cls.__LOG_FILE_NAME

	@staticmethod
	def do_something_strange(a, b):
		print(a * b)



def main():
	test_case = SomeMagicTestCase()
	test_case2 = SomeMagicTestCase(666)

	test_case.say_hello()
	test_case.do_magic()

	# SomeMagicTestCase.change_log_file_name("ROFL")
	SomeMagicTestCase.change_log_file_name("ROFL333")
	SomeMagicTestCase.do_something_strange(6, 7)

	lol = SomeMagicTestCase.get_log_file_name()
	print(lol)

	# SomeMagicTestCase.__LOG_FILE_NAME = "FUBAR.txt"
	# print(test_case.__LOG_FILE_NAME)
	# # print(test_case.LOG_FILE_NAME33)
	# print(test_case2.__LOG_FILE_NAME)
	# test_case.a = 999
	# test_case2.a = 233433

	# test_case.say_hello()
	# test_case2.say_hello()

	# lol = []
	# for i in range(0, 10):
	# 	tt = SomeMagicTestCase()
	# 	tt.a = i ** 2
	# 	lol.append(tt)

	# for i in range(0, 10):
	# 	lol[i].say_hello()



if __name__ == "__main__":
	main()
