import re

frequency = {}

document_text = open('input.txt','r')
# делаем все буквы в нашем документе строчными, чтобы легче было применить регулярное выражение
text_string = document_text.read().lower()
# реализуем поиск на основе регулярного выражения (созданный шаблон подразумевает поиск слов длинной от 3-х букв
# написанных латинскими строчными буквами)
match_pattern = re.findall(r'\b[a-z]{1,15}\b', text_string)
# с помощью цикла for осуществляем подсчёт количества повторений слов в тексте, с последующей записью результатов
# в словарь frequency{}
for word in match_pattern:
    count = frequency.get(word, 0)
    frequency[word] = count + 1
# создаем список ключей, отсортированный по значению словаря frequency{}
sorted_keys = sorted(frequency, key=lambda x: int(frequency[x]), reverse=True)
file = open('output.txt','w')
# с помощью цикла for выбираем и форматируем данные(слово + кол-во его повторений в тексте) для последующего
# сохранения результатов в файл output.txt
for key in sorted_keys:
    s = str('{}->{}\n').format(key,frequency[key])
    file.write(s)
print('Результат записан в:','output.txt')

# реализуем консольный ввод количества выводимых, наиболее часто встречающихся слов
quantity = int(input('Введите количество наиболее часто встречающихся слов: '))
file = open('output.txt','r')
lines = file.readlines()
print(lines[0:quantity])
file.close()




