def print_menu():
    print('MENU')
    print('1. Add new entry')
    print('2. Load entrys from file')
    print('3. Print all entrys')
    print('4. Delete entry')
    print('5. Save to file')
    print('6. Search by ID')
    print('7. Search by name')    
    print('8. Exit')

BD = {}
list_from_file = []
pointer = 0
del_id = ' '



print_menu()
while pointer != 8:
    pointer = int(input("Type a command number: "))
    if pointer == 1:
        print("Add new entry")
        ID = input("ID: ")
        name = input("Name: ")
        phone = input("Number: ")
        BD[ID] = [name,phone]

    elif pointer == 2:
        print("Enter file name:")

        with open(str(input())) as infile:
            for line in infile:
                line = line.split()
                key = line[0]
                entry = [line[1],line[2]]
                BD[key] = entry
        

    elif pointer == 3:
        print("Telephone Numbers:")
        for x in BD.keys():
            print("ID: ", x, "\tName: {} \tNumber: {}".format(*BD[x]))
        print()

    elif pointer == 4:
        print("Remove entry")
        entry_to_remove = input("Name or ID: ")

        for x,y in BD.items():
            if x == entry_to_remove:
                del_id = x
            elif y[0] == entry_to_remove:
                del_id = x
                
        if del_id in BD.keys():
            del BD[del_id]
        else:
            print ("Entry wasn't found")

    elif pointer == 5:
        print("Enter file name:")

        with open(str(input()),"w") as savefile:
            for x,y in BD.items():
                savefile.write("ID: %s\tName: %s \tNumber: %s" %(x,y[0],y[1]))

    elif pointer == 6:
        print("Lookup Number")
        name = input("ID: ")
        if ID in BD:
            print("The number is", BD[name])
        else:
            print("ID was not found")

    elif pointer == 7:
        print("Lookup Number")
        name = input("Name: ")
        
        for x,y in BD.items():
            if y[0] == name:
                found_id = x

        if found_id in BD.keys():
            print("The number is", BD[found_id][1])
        else:
            print("Name was not found")

    elif pointer > 8:
        print("Invalid command")
    elif pointer != 8:
        print_menu()
