# coding=utf-8
alice = open('input.txt', 'r')
wordcount = {}
wc = []
bookflag = 0

# Копипаста для сортировки

class wordnum(object):
    def __init__(self, word, num):
        self.word = word
        self.num = num

    def __repr__(self):
        return "" % (self.word, self.num)

def byNum(wordnum):
    return wordnum.num

# Форматирование для этой книги

for byte_line in alice:
    if 'START OF THIS PROJECT GUTENBERG' in byte_line:
        bookflag = 1
        break
if bookflag == 0:
    print("Начало книги не найдено. За Начало принято начало файла. \n")
    alice.close()
    alice = open('input.txt', 'r')
bookflag = 0
for byte_line in alice:
    line = str(byte_line)

    if 'End of Project Gutenberg’s' in byte_line:
        bookflag= 1
        break

    # Приведем все буквы к нижнему регистру, уберём пунктуацию и разобьём каждую строчку на слова

    line1 = line.lower()
    # Фильтр пунктуации. Изначально хотела привести 'll к will, но встретилась с Alice's в значении "чей".
    # Допускаю, что слово с апострофом в середине - одно уникальное слово
    line2 = line1.replace('--', ' ').replace('-', '').replace('.', '').replace('!', '').replace('?', '').replace(':', '').replace(';','').replace(
        '"', '').replace("'", "").replace('*', '').replace('(', '').replace(')', '').replace(',', '').replace(
        '’ ',' ').replace('‘', ' ').replace('”', ' ').replace('“', ' ').replace('’\n','').replace('”\n','')

    line_list = line2.split()
    for word in line_list:
        if wordcount.get(word) == None:
            wordcount.update({word: 1})
        else:
            num = wordcount[word]
            wordcount.update({word: num+1})
        if word == 'for”':
            print(line1)

if bookflag == 0:
    print("Конец книги не найден. За Конец принят конец файла. \n")

for key in wordcount:
    wc.append(wordnum(key, wordcount[key]))

swc = sorted(wc, key = byNum, reverse=True)
rfile = open('result.txt', 'a')
for i in swc:
    rfile.write(i.word + ' ' + str(i.num) + '\n')
rfile.close()

print ("Найдено " + str(len(swc)) + " слов. Сколько самых популярных слов показать?\n")

j=input()
while j.isnumeric() == False or int(j)>len(swc):
    print("Попробуй ещё\n")
    j=input()
g=int(j)
for i in swc:
    if g==0:
        break
    print(i.word)
    print(i.num)
    g-=1
