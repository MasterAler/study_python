try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description': 'My first module',
    'author': 'geek',
    'url': 'URL to get it at.',
    'download_url': 'Where to download it.',
    'author_email': 'noname@mail.ru',
    'version': '0.1',
    'install_requires': ['requests', 'nose'],
    'packages': ['my_logger'],
    'scripts': [],
    'name': 'my_first_logger'
}

setup(**config)